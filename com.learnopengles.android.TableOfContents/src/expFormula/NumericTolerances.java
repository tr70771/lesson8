package expFormula;
/**
 * The NumericTolerances is simply a structure that holds information about 
 * zero tolerances (i.e. when a small number should be considered to be 0)
 * and equality tolerances (i.e. when two numbers should be considered equal).
 * These facilities can be used to help combat round-off error in approximations.
 * 
 * <P>
 * The NumericTolerances class has six private fields:<BR><BR> 
 * 	private boolean useZeroTol; - determines if the zero tolerance should be used.<BR> 
 * 	private int ZeroExp; - exponent of 10 to be used for the zero tolerance. <BR> 
 * 	private double ZeroTol; - 10^ZeroExp.<BR> 
 * 	private boolean useEqualityTol; - determines if the equality tolerance should 
 * be used.<BR> 
 * 	private int EqualityExp; - exponent of 10 to be used for the equality tolerance.<BR> 
 * 	private double EqualityTol; - 10^EqualityExp.<BR> 
 * 
 * @author Don Spickler
 * @version 1.1
 */
public class NumericTolerances implements Cloneable
{
	private boolean useZeroTol;
	private int ZeroExp;
	private double ZeroTol;
	private boolean useEqualityTol;
	private int EqualityExp;
	private double EqualityTol;

	/**
	 * Default constructor that sets both useZeroTol and useEqualityTol to true, 
	 * both exponents to -8 and both tolerances to 10^(-8).  
	 */
	public NumericTolerances() 
	{	
		useZeroTol = true;
		ZeroExp = -8;
		ZeroTol = Math.pow(10, ZeroExp);
		useEqualityTol = true;
		EqualityExp = -8;
		EqualityTol = Math.pow(10, EqualityExp);
	}

	/**
	 * This will return a NumericTolerances object that represents exact arithmetic.
	 * That is, it sets the useZeroTol and the useEqualityTol fields to false.
	 * @return NumericTolerances object that represents exact arithmetic.
	 */
	public static NumericTolerances setForExactNumber()
	{
		NumericTolerances retval = new NumericTolerances();
		retval.setUseZeroTolerance(false);
		retval.setUseEqualityTolerance(false);
		
		return retval;
	}

	/**
	 * Returns the ZeroExp. 
	 * @return ZeroExp
	 */
	public int getZeroToleranceExponent()
	{
		return ZeroExp;
	}

	/**
	 * Returns the EqualityExp. 
	 * @return EqualityExp
	 */
	public int getEqualityToleranceExponent()
	{
		return EqualityExp;
	}

	/**
	 * Returns the zero tolerance, ZeroTol.
	 * @return The zero tolerance, ZeroTol.
	 */
	public double getZeroTolerance()
	{
		return ZeroTol;
	}

	/**
	 * Returns the equality tolerance, EqualityTol.
	 * @return The equality tolerance, EqualityTol.
	 */
	public double getEqualityTolerance()
	{
		return EqualityTol;
	}
	
	/**
	 * Returns the boolean useZeroTol. 
	 * @return true if the useZeroTol is true and false otherwise.
	 */
	public boolean getUseZeroTolerance()
	{
		return useZeroTol;
	}

	/**
	 * Returns the boolean useEqualityTol. 
	 * @return true if the useEqualityTol is true and false otherwise.
	 */
	public boolean getUseEqualityTolerance()
	{
		return useEqualityTol;
	}

	/**
	 * Sets the zero exponent, ZeroExp, to exponent and the zero tolerance, ZeroTol,
	 * to 10^exponent.
	 * @param exponent the exponent to use for the zero tolerance.  
	 */
	public void setZeroToleranceExponent(int exponent)
	{
		if (exponent < -1)
		{
			ZeroExp = exponent;
			ZeroTol = Math.pow(10.0, (double)ZeroExp);
		}
	}

	/**
	 * Sets the equality exponent, EqualityExp, to exponent and the equality tolerance, 
	 * EqualityTol, to 10^exponent.
	 * @param exponent the exponent to use for the equality tolerance.  
	 */
	public void setEqualityToleranceExponent(int exponent)
	{
		if (exponent < -1)
		{
			EqualityExp = exponent;
			EqualityTol = Math.pow(10.0, (double)EqualityExp);
		}
	}

	/**
	 * Sets the useZeroTol to b.
	 * @param b boolean to set useZeroTol to.
	 */
	public void setUseZeroTolerance(boolean b)
	{
		useZeroTol = b;
	}

	/**
	 * Sets the useEqualityTol to b.
	 * @param b boolean to set useEqualityTol to.
	 */
	public void setUseEqualityTolerance(boolean b)
	{
		useEqualityTol = b;
	}

	/**
	 * Returns a NumericTolerances object that is a copy of the current object.
	 * @return A NumericTolerances object that is a copy of the current object.
	 */
	public NumericTolerances clone()
	{
		NumericTolerances retval = new NumericTolerances();
		
		retval.useZeroTol = useZeroTol;
		retval.ZeroExp = ZeroExp;
		retval.ZeroTol = ZeroTol;
		retval.useEqualityTol = useEqualityTol;
		retval.EqualityExp = EqualityExp;
		retval.EqualityTol = EqualityTol;
		
		return retval;
	}


}
