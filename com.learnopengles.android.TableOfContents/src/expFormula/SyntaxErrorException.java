package expFormula;
/**
 * This is a simple extension of the RuntimeException class.
 * @author Don Spickler
 * @version 1.1
 */

public class SyntaxErrorException extends RuntimeException 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Default constructor.
	 */
	public SyntaxErrorException()
	{
		super("");
	}
	
	/**
	 * Constructor that allows the input of an error string.
	 * @param s the error string.
	 */
	public SyntaxErrorException(final String s)
	{
		super(s);
	}	
}
