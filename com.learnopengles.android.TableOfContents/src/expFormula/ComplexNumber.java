package expFormula;
import java.math.*;
import java.lang.Math;

/**
 * The ComplexNumber class is for implementing complex number arithmetic
 * into your programs.  It has two private fields, real and imag that each hold 
 * a {@link NumberValue} type which is a special class for real numbers
 * that allows you to store exact (infinite precision) rational numbers or doubles.
 * The class also has built-in methods for complex number arithmetic, logs, 
 * exponentials, trigonometric, inverse trigonometric, hyperbolic and
 * inverse hyperbolic functions.  Complex numbers are stored in rectangular form.      
 * 
 * @author Don Spickler
 * @version 1.1
 */

public class ComplexNumber implements Cloneable 
{
/**
 * The real part of the complex number.
 */
	private NumberValue real = null;

/**
 *  The imaginary part of the complex number.
 */
	private NumberValue imag = null;
	
/**
 * Constructor that takes two {@link NumberValue} parameters.  
 * @param r NumberValue type that will be stored in the real part of the complex
 * number.
 * @param i NumberValue type that will be stored in the imaginary part of the 
 * complex number.
 */
	public ComplexNumber(final NumberValue r, final NumberValue i)
	{
		real = r;
		imag = i;
	}

/**
 * Constructor that should be used if you wish to work with real numbers,  It will
 * automatically set the imaginary part of the complex number to 0.
 * @param r NumberValue type that will be stored in the real part of the complex
 * number.
 */
	public ComplexNumber(final NumberValue r)
	{
		real = r;
		imag = NumberValue.makeZero();
	}

/**
 * Constructor that can be used for either purely real or imaginary numbers. 
 * @param r NumberValue type that will be stored in either the real part or
 * the imaginary part of the complex number, depending on the realflag value.
 * @param realflag boolean that determines where the number is stored.  If true
 * the number will be stored in the real part of the complex number and the
 * imaginary part will be set to 0.  If false then number will be stored in 
 * the imaginary part of the complex number and the
 * real part will be set to 0.
 */
	public ComplexNumber(final NumberValue r, final boolean realflag)
	{
		if (realflag)
		{
			real = r;
			imag = NumberValue.makeZero();
		}
		else
		{
			real = NumberValue.makeZero();
			imag = r;
		}		
	}

/**
 * Constructor for inputting two long integers for the real and imaginary parts 
 * of the complex number.  Parameters will automatically be converted to 
 * {@link NumberValue} types in exact mode.
 * @param r long integer that will be converted to a NumberValue type in exact mode
 * and stored in the real part of the complex number. 
 * @param i long integer that will be converted to a NumberValue type in exact mode
 * and stored in the imaginary part of the complex number.
 */
	public ComplexNumber(final long r, final long i)
	{
		real = new NumberValue(r, 1);
		imag = new NumberValue(i, 1);
	}

/**
 * Constructor for inputting four long integers for the real and imaginary parts
 * of the complex number.  Parameters will automatically be converted to 
 * {@link NumberValue} types in exact mode.
 * 
 * @param rn long integer that will be used as the numerator in a NumberValue type 
 * in exact mode and stored in the real part of the complex number.
 * @param rd long integer that will be used as the denominator in a NumberValue type 
 * in exact mode and stored in the real part of the complex number.
 * @param in long integer that will be used as the numerator in a NumberValue type 
 * in exact mode and stored in the imaginary part of the complex number.
 * @param id long integer that will be used as the denominator in a NumberValue type 
 * in exact mode and stored in the imaginary part of the complex number.
 */
	public ComplexNumber(final long rn, final long rd, final long in, final long id)
	{
		real = new NumberValue(rn, rd);
		imag = new NumberValue(in, id);
	}

/**
 * Constructor for inputting two doubles for the real and imaginary parts 
 * of the complex number.  Parameters will automatically be converted to 
 * {@link NumberValue} types in approximate mode.
 * 	
 * @param r double that will be converted to a NumberValue type in approximate mode
 * and stored in the real part of the complex number.
 * @param i double that will be converted to a NumberValue type in approximate mode
 * and stored in the imaginary part of the complex number.
 */
	public ComplexNumber(final double r, final double i)
	{
		real = new NumberValue(r);
		imag = new NumberValue(i);
	}

/**
 * Default constructor for a complex number, both the real and imaginary parts 
 * of the complex number will be 0.
 */
	public ComplexNumber()
	{
		real = NumberValue.makeZero();
		imag = NumberValue.makeZero();
	}
	
/**
 * Returns a ComplexNumber type that is 0 in exact mode.
 * @return ComplexNumber type that is 0 in exact mode.
 */
	public static ComplexNumber makeZero()
	{
		return new ComplexNumber(NumberValue.makeZero(), NumberValue.makeZero());
	}

/**
 * Returns a ComplexNumber type that is 1 in exact mode.
 * @return ComplexNumber type that is 1 in exact mode.
 */
	public static ComplexNumber makeOne()
	{
		return new ComplexNumber(NumberValue.makeOne(), NumberValue.makeZero());
	}

/**
 * Returns a ComplexNumber type that is -1 in exact mode.
 * @return ComplexNumber type that is -1 in exact mode.
 */
	public static ComplexNumber makeMinusOne()
	{
		return new ComplexNumber(NumberValue.makeMinusOne(), NumberValue.makeZero());
	}

/**
 * Returns a ComplexNumber type that is i in exact mode.
 * @return ComplexNumber type that is i in exact mode.
 */
	public static ComplexNumber makeI()
	{
		return new ComplexNumber(NumberValue.makeZero(), NumberValue.makeOne());
	}

/**
 * Returns a ComplexNumber type that is -i in exact mode.
 * @return ComplexNumber type that is -i in exact mode.
 */
	public static ComplexNumber makeMinusI()
	{
		return new ComplexNumber(NumberValue.makeZero(), NumberValue.makeMinusOne());
	}

/**
 * Forces the imaginary part of the complex number to be 0.
 */
	private void makeReal()
	{
		imag = NumberValue.makeZero();
	}

/**
 * Gets the real part of the complex number.
 * @return NumberValue type of the real part of the complex number.
 */
	public NumberValue getReal()
	{
		return real;
	}

/**
 * Gets the imaginary part of the complex number.
 * @return NumberValue type of the imaginary part of the complex number.
 */
	public NumberValue getImag()
	{
		return imag;
	}

/**
 * Determines if the complex number is real or not.
 * @return true if the imaginary part is 0 and false otherwise.
 */
	public boolean isReal()
	{
		return imag.isZero();
	}

/**
 * Determines if the complex number is purely imaginary or not.
 * @return true if the real part is 0 and false otherwise.
 */
	public boolean isImag()
	{
		return real.isZero();
	}

/**
 * Determines if the complex number is both real and the real part is an integer.
 * @return true if the imaginary part is 0 and the real part is an exact integer
 * and false otherwise.
 */
	public boolean isRealInteger()
	{
		return imag.isZero() && real.isInteger();
	}

/**
 * Determines if the complex number is exactly 0.
 * @return true if the complex number is exactly 0 and false otherwise.
 */
	public boolean isZero()
	{
		if (real.isExact() && imag.isExact())
			return (real.isZero() && imag.isZero());
			
		return (mod() == 0);
	}

/**
 * Determines if the complex number is 0 under the input {@link NumericTolerances}.
 * @param tol NumericTolerances type used to determine if the complex number is 0.
 * @return true if the complex number is 0 under the restrictions of the 
 * input NumericTolerances and false otherwise.
 */
	public boolean isZero(NumericTolerances tol)
	{
		if (real.isExact() && imag.isExact())
			return (real.isZero(tol) && imag.isZero(tol));
			
		if (tol.getUseZeroTolerance())
			return (mod() < tol.getZeroTolerance());
		else
			return (mod() == 0);
	}

/**
 * Determines if the complex number is exactly 1.
 * @return true if the complex number is exactly 1 and false otherwise.
 */
	public boolean isOne()
	{
		if (real.isExact() && imag.isExact())
			return (real.isOne() && imag.isZero());

		return (this.subtract(ComplexNumber.makeOne()).mod() == 0);
	}

/**
 * Determines if the complex number is 1 under the input {@link NumericTolerances}.
 * @param tol NumericTolerances type used to determine if the complex number is 1.
 * @return true if the complex number is 1 under the restrictions of the 
 * input NumericTolerances and false otherwise.
 */
	public boolean isOne(NumericTolerances tol)
	{
		if (real.isExact() && imag.isExact())
			return (real.isOne(tol) && imag.isZero(tol));

		if (tol.getUseEqualityTolerance())
			return (this.subtract(ComplexNumber.makeOne()).mod() < tol.getEqualityTolerance());
		else
			return (this.subtract(ComplexNumber.makeOne()).mod() == 0);
	}

/**
 * Determines if the complex number is exactly -1.
 * @return true if the complex number is exactly -1 and false otherwise.
 */
	public boolean isMinusOne()
	{
		if (real.isExact() && imag.isExact())
			return (real.isMinusOne() && imag.isZero());

		return (this.subtract(ComplexNumber.makeMinusOne()).mod() == 0);
	}

/**
 * Determines if the complex number is -1 under the input {@link NumericTolerances}.
 * @param tol NumericTolerances type used to determine if the complex number is -1.
 * @return true if the complex number is -1 under the restrictions of the 
 * input NumericTolerances and false otherwise.
 */
	public boolean isMinusOne(NumericTolerances tol)
	{
		if (real.isExact() && imag.isExact())
			return (real.isMinusOne(tol) && imag.isZero(tol));

		if (tol.getUseEqualityTolerance())
			return (this.subtract(ComplexNumber.makeMinusOne()).mod() < tol.getEqualityTolerance());
		else
			return (this.subtract(ComplexNumber.makeMinusOne()).mod() == 0);
	}

/**
 * Determines if the first term in the complex number is negative.  This method is 
 * mainly used in the toString() method of classes that use the ComplexNumber 
 * class to make the output cleaner and easier to read. 
 * @return true if the first term is negative and false otherwise.  For example, 
 * -2+3i, -4, and -7i would all return true.
 */
	public boolean hasNegativeFirstTerm()
	{
		if (!real.isZero())
			return !real.isPos();
		else if (!imag.isZero())
			return !imag.isPos();
		else
			return false;
	}

/**
 * Determines if the complex number has two non-zero terms.
 * @return true if both the real and imaginary parts are not zero.
 */
	public boolean hasTwoTerms()
	{
		return (!real.isZero()) && (!imag.isZero());
	}

/**
 * Approximates both the real and imaginary parts of the complex number and 
 * returns a new ComplexNumber object with those values.
 * @return A ComplexNumber object where both the real and imaginary parts have been
 * approximated.
 */
	public ComplexNumber force_approx()
	{
		return new ComplexNumber(new NumberValue(real.approx()), new NumberValue(imag.approx()));
	}	

/**
 * Approximates both the real and imaginary parts of the complex number and 
 * returns a new ComplexNumber object with those values.  If either the real 
 * or imaginary parts are exactly integers then that part remains exact.  The 
 * difference between this method and the {@link #force_approx() force_approx()} method is that the 
 * force_approx() method will convert the exact integer to an approximate double.   
 * @return A ComplexNumber object where both the real and imaginary parts have been
 * approximated unless the part is exactly an integer, in which case the part remains 
 * an exact integer.
 */
	public ComplexNumber approx()
	{
		NumberValue newReal;
		NumberValue newImag;
		
		if (real.isInteger())
			newReal = real.clone();
		else
			newReal = new NumberValue(real.approx());
			
		if (imag.isInteger())
			newImag = imag.clone();
		else
			newImag = new NumberValue(imag.approx());
			
		return new ComplexNumber(newReal, newImag);
	}	

/**
 * Adds the complex number n to the current complex number and returns the result.	
 * @param n a ComplexNumber type.
 * @return this + n
 * @throws ArithmeticException
 */
	public ComplexNumber add(ComplexNumber n)
	{
		ComplexNumber retval = null;
		
		try
		{
			retval = new ComplexNumber(real.add(n.getReal()), imag.add(n.getImag()));
		} catch (Exception e)
		{
			throw new ArithmeticException("ComplexNumber: add method numeric exception.");
		}

		return retval;
	}

/**
 * Subtracts the complex number n from the current complex number and returns the 
 * result.	
 * @param n a ComplexNumber type.
 * @return this - n
 * @throws ArithmeticException
 */
	public ComplexNumber subtract(ComplexNumber n)
	{
		ComplexNumber retval = null;
		
		try
		{
			retval = new ComplexNumber(real.subtract(n.getReal()), imag.subtract(n.getImag()));
		} catch (Exception e)
		{
			throw new ArithmeticException("ComplexNumber: subtract method numeric exception.");
		}

		return retval;
	}

/**
 * Negates the current complex number and returns the result.	
 * @return -this
 * @throws ArithmeticException
 */
	public ComplexNumber negate()
	{
		ComplexNumber retval = null;
		
		try
		{
			retval = new ComplexNumber(real.negate(), imag.negate());
		} catch (Exception e)
		{
			throw new ArithmeticException("ComplexNumber: negate method numeric exception.");
		}

		return retval;
	}

/**
 * Returns the complex conjugate of the current complex number and returns the result.	
 * @return if this is a + bi it returns a - bi.
 * @throws ArithmeticException
 */
	public ComplexNumber conj()
	{
		ComplexNumber retval = null;
		
		try
		{
			retval = new ComplexNumber(real.clone(), imag.negate());
		} catch (Exception e)
		{
			throw new ArithmeticException("ComplexNumber: conj method numeric exception.");
		}

		return retval;
	}
	
/**
 * Multiplies the complex number n and the current complex number and returns the 
 * result.	
 * @param n a ComplexNumber type.
 * @return this * n
 * @throws ArithmeticException
 */
	public ComplexNumber multiply(ComplexNumber n)
	{
		ComplexNumber retval = null;
		
		try
		{
			retval = new ComplexNumber((real.multiply(n.getReal())).subtract(imag.multiply(n.getImag())), (real.multiply(n.getImag())).add(imag.multiply(n.getReal())));
		} catch (Exception e)
		{
			throw new ArithmeticException("ComplexNumber: multiply method numeric exception.");
		}

		return retval;
	}
	
/**
 * Divides the complex number n into the current complex number and returns the 
 * result.	
 * @param n a ComplexNumber type.
 * @return this / n
 * @throws ArithmeticException
 */
	public ComplexNumber divide(ComplexNumber n)
	{
		ComplexNumber retval = null;

		try
		{
			NumberValue den = ((n.getReal()).multiply(n.getReal())).add((n.getImag()).multiply(n.getImag()));
			NumberValue r = (real.multiply(n.getReal())).add(imag.multiply(n.getImag()));
			NumberValue i = (imag.multiply(n.getReal())).subtract(real.multiply(n.getImag()));			
			r = r.divide(den);
			i = i.divide(den);
			retval = new ComplexNumber(r, i);
		} catch (Exception e)
		{
			throw new ArithmeticException("ComplexNumber: divide method numeric exception.");
		}

		return retval;
	}

/**
 * Divides the double d into the current complex number and returns the 
 * result.	
 * @param d a double.
 * @return this / d
 * @throws ArithmeticException
 */
	public ComplexNumber divide(double d)
	{
		ComplexNumber retval = null;

		try
		{
			NumberValue den = new NumberValue(d);			
			NumberValue r = real.divide(den);
			NumberValue i = imag.divide(den);
			retval = new ComplexNumber(r, i);
		} catch (Exception e)
		{
			throw new ArithmeticException("ComplexNumber: divide method numeric exception.");
		}

		return retval;
	}

/**
 * Inverts the current complex number and returns the result.	
 * @return 1 / this
 * @throws ArithmeticException
 */
	public ComplexNumber invert()
	{
		ComplexNumber retval = null;
		
		try
		{
			ComplexNumber one = ComplexNumber.makeOne();
			retval = one.divide(this);
		} catch (Exception e)
		{
			throw new ArithmeticException("ComplexNumber: invert method numeric exception.");
		}

		return retval;
	}

/**
 * Raises the current complex number to the integer (long) power of p and returns 
 * the result. Note that the expression 0^0 returns a 1.  Although mathematically 
 * incorrect it is needed by the {@link ExpFormula ExpFormula} class in the 
 * simplification methods for polynomials.  
 * @param p long integer power.
 * @return this ^ p
 * @throws ArithmeticException
 */
	public ComplexNumber pow(long p)
	{
		ComplexNumber retval = ComplexNumber.makeOne();
		ComplexNumber base = clone();

		if (isZero())
		{
			if (p == 0)
				return ComplexNumber.makeOne();  //  0^0 = 1, needed for polynomial simplifications.
			else if (p > 0)
				return ComplexNumber.makeZero();
			else if (p < 0)
				throw new ArithmeticException("Division by zero.");
		}
		
		if (isOne())
			return ComplexNumber.makeOne();
		
		if (p < 0)
		{
			base.invert();
			p = -p;
		}

		try
		{
			for (int i = 0; i < p; i++)
				retval = retval.multiply(base);
		} catch (Exception e)
		{
			throw new ArithmeticException("ComplexNumber: pow(int) method numeric exception.");
		}

		return retval;
	}

/**
 * Raises the current complex number to the complex number power of p and returns 
 * the result. Note that the expression 0^0 returns a 1.  Although mathematically 
 * incorrect it is needed by the {@link ExpFormula ExpFormula} class in the 
 * simplification methods for polynomials.  
 * @param p ComplexNumber type used for the power.
 * @return this ^ p
 * @throws ArithmeticException
 */
	public ComplexNumber pow(ComplexNumber p)
	{
		ComplexNumber retval = new ComplexNumber();
		boolean done = false;
		
		try
		{
			if (p.isReal() && p.real.isExact() && p.real.isInteger())
			{
				retval = this.pow(p.real.getNum().longValue());
				done = true;
			}
			
			if (isReal() && p.isReal() && !done)
				if (p.real.isExact())
				{
					BigInteger pnum = p.real.getNum();
					BigInteger pden = p.real.getDen();
					
					if (real.isPos())
					{						
						retval = new ComplexNumber(Math.pow(real.approx(), p.real.approx()), 0);
						done = true;
					}
					else if (real.isNeg() && (pden.mod(new BigInteger("2")).intValue() == 1))
					{
						retval = new ComplexNumber(Math.pow(-1.0*Math.pow(real.negate().approx(), 1/pden.doubleValue()),pnum.doubleValue()), 0);
						done = true;						
					}
					else if (real.isZero() && p.real.isPos()) // zero
					{
						retval = new ComplexNumber(0, 0);
						done = true;						
					}
				}
			
			if (!done)
				retval = (p.multiply(log())).exp();  //  general complex to complex
		} catch (Exception e)
		{
			throw new ArithmeticException("ComplexNumber: pow(ComplexNumber) method numeric exception.");
		}
		
		return retval;
	}

	//  Functions

/**
 * Returns the natural log of the current complex number, ln(|z|) + arg(z)i.  
 * If the result is real then the method will force the imaginary part to 0
 * to reduce roundoff error.
 * @return The natural log of this.
 * @throws ArithmeticException
 */
	public ComplexNumber log()   //  log(this)
	{
		ComplexNumber retval = new ComplexNumber();
		
		try
		{
			retval = new ComplexNumber(Math.log(mod()), arg());
			if (isReal() && (real.approx() > 0)) retval.makeReal();
		} catch (Exception e)
		{
			throw new ArithmeticException("ComplexNumber: log method numeric exception.");
		}
		
		return retval;
	}
	
/**
 * Returns the exponential of the current complex number, 
 * if z = a + bi then e^a cos(b) + (e^a sin(b))i.  
 * If the result is real then the method will force the imaginary part to 0
 * to reduce roundoff error.
 * @return The exponential of this.
 * @throws ArithmeticException
 */
	public ComplexNumber exp()   //  exp(this)
	{
		ComplexNumber retval = new ComplexNumber();
		
		try
		{
			retval = new ComplexNumber(Math.exp(real.approx())*Math.cos(imag.approx()), 
																 Math.exp(real.approx())*Math.sin(imag.approx()));
			if (isReal()) retval.makeReal();
		} catch (Exception e)
		{
			throw new ArithmeticException("ComplexNumber: exp method numeric exception.");
		}
		
		return retval;
	}
	
/**
 * Returns the sine of the current complex number, 
 * (e^(iz)-e^(-iz))/(2i).  
 * If the result is real then the method will force the imaginary part to 0
 * to reduce roundoff error.
 * @return The sine of this.
 * @throws ArithmeticException
 */
	public ComplexNumber sin()   //  sin(this)
	{
		ComplexNumber retval = new ComplexNumber();
		ComplexNumber two = new ComplexNumber(new NumberValue("2", "1"));
		
		try
		{
			retval = (((this.multiply(ComplexNumber.makeI())).exp()).subtract((this.multiply(ComplexNumber.makeMinusI())).exp())).divide(two.multiply(ComplexNumber.makeI()));
			if (isReal()) retval.makeReal();
		} catch (Exception e)
		{
			throw new ArithmeticException("ComplexNumber: sin method numeric exception.");
		}
		
		return retval;
	}
	
/**
 * Returns the cosine of the current complex number, 
 * (e^(iz)+e^(-iz))/2.  
 * If the result is real then the method will force the imaginary part to 0
 * to reduce roundoff error.
 * @return The cosine of this.
 * @throws ArithmeticException
 */
	public ComplexNumber cos()   //  cos(this)
	{
		ComplexNumber retval = new ComplexNumber();
		ComplexNumber two = new ComplexNumber(new NumberValue("2", "1"));
		
		try
		{
			retval = (((this.multiply(ComplexNumber.makeI())).exp()).add((this.multiply(ComplexNumber.makeMinusI())).exp())).divide(two);
			if (isReal()) retval.makeReal();
		} catch (Exception e)
		{
			throw new ArithmeticException("ComplexNumber: cos method numeric exception.");
		}
		
		return retval;
	}
	
/**
 * Returns the tangent of the current complex number, 
 * sin(z) / cos(z).  
 * If the result is real then the method will force the imaginary part to 0
 * to reduce roundoff error.
 * @return The tangent of this.
 * @throws ArithmeticException
 */
	public ComplexNumber tan()   //  tan(this)
	{
		ComplexNumber retval = new ComplexNumber();
		
		try
		{
			retval = (sin()).divide(cos());
			if (isReal()) retval.makeReal();
		} catch (Exception e)
		{
			throw new ArithmeticException("ComplexNumber: tan method numeric exception.");
		}
		
		return retval;
	}

/**
 * Returns the cotangent of the current complex number, 
 * cos(z) / sin(z).  
 * If the result is real then the method will force the imaginary part to 0
 * to reduce roundoff error.
 * @return The cotangent of this.
 * @throws ArithmeticException
 */
	public ComplexNumber cot()   //  cot(this)
	{
		ComplexNumber retval = new ComplexNumber();
		
		try
		{
			retval = (cos()).divide(sin());
			if (isReal()) retval.makeReal();
		} catch (Exception e)
		{
			throw new ArithmeticException("ComplexNumber: cot method numeric exception.");
		}
		
		return retval;
	}
	
/**
 * Returns the secant of the current complex number, 
 * 1 / cos(z).  
 * If the result is real then the method will force the imaginary part to 0
 * to reduce roundoff error.
 * @return The secant of this.
 * @throws ArithmeticException
 */
	public ComplexNumber sec()   //  sec(this)
	{
		ComplexNumber retval = new ComplexNumber();
		
		try
		{
			retval = (ComplexNumber.makeOne()).divide(cos());
			if (isReal()) retval.makeReal();
		} catch (Exception e)
		{
			throw new ArithmeticException("ComplexNumber: sec method numeric exception.");
		}
		
		return retval;
	}
	
/**
 * Returns the cosecant of the current complex number, 
 * 1 / sin(z).  
 * If the result is real then the method will force the imaginary part to 0
 * to reduce roundoff error.
 * @return The cosecant of this.
 * @throws ArithmeticException
 */
	public ComplexNumber csc()   //  csc(this)
	{
		ComplexNumber retval = new ComplexNumber();
		
		try
		{
			retval = (ComplexNumber.makeOne()).divide(sin());
			if (isReal()) retval.makeReal();
		} catch (Exception e)
		{
			throw new ArithmeticException("ComplexNumber: csc method numeric exception.");
		}
		
		return retval;
	}
	
/**
 * Returns the hyperbolic sine of the current complex number, 
 * (e^z-e^(-z))/2.  
 * If the result is real then the method will force the imaginary part to 0
 * to reduce roundoff error.
 * @return The hyperbolic sine of this.
 * @throws ArithmeticException
 */
	public ComplexNumber sinh()   //  sinh(this)
	{
		ComplexNumber retval = new ComplexNumber();
		ComplexNumber two = new ComplexNumber(new NumberValue("2", "1"));
		
		try
		{
			retval = ((this.exp()).subtract((this.multiply(ComplexNumber.makeMinusOne())).exp())).divide(two);
			if (isReal()) retval.makeReal();
		} catch (Exception e)
		{
			throw new ArithmeticException("ComplexNumber: sinh method numeric exception.");
		}
		
		return retval;
	}
	
/**
 * Returns the hyperbolic cosine of the current complex number, 
 * (e^z+e^(-z))/2.  
 * If the result is real then the method will force the imaginary part to 0
 * to reduce roundoff error.
 * @return The hyperbolic cosine of this.
 * @throws ArithmeticException
 */
	public ComplexNumber cosh()   //  cosh(this)
	{
		ComplexNumber retval = new ComplexNumber();
		ComplexNumber two = new ComplexNumber(new NumberValue("2", "1"));
		
		try
		{
			retval = ((this.exp()).add((this.multiply(ComplexNumber.makeMinusOne())).exp())).divide(two);
			if (isReal()) retval.makeReal();
		} catch (Exception e)
		{
			throw new ArithmeticException("ComplexNumber: cosh method numeric exception.");
		}
		
		return retval;
	}
	
/**
 * Returns the hyperbolic tangent of the current complex number, 
 * sinh(z) / cosh(z).  
 * If the result is real then the method will force the imaginary part to 0
 * to reduce roundoff error.
 * @return The hyperbolic tangent of this.
 * @throws ArithmeticException
 */
	public ComplexNumber tanh()   //  tanh(this)
	{
		ComplexNumber retval = new ComplexNumber();
		
		try
		{
			retval = (sinh()).divide(cosh());
			if (isReal()) retval.makeReal();
		} catch (Exception e)
		{
			throw new ArithmeticException("ComplexNumber: tanh method numeric exception.");
		}
		
		return retval;
	}
	
/**
 * Returns the hyperbolic cotangent of the current complex number, 
 * cosh(z) / sinh(z).  
 * If the result is real then the method will force the imaginary part to 0
 * to reduce roundoff error.
 * @return The hyperbolic cotangent of this.
 * @throws ArithmeticException
 */
	public ComplexNumber coth()   //  coth(this)
	{
		ComplexNumber retval = new ComplexNumber();
		
		try
		{
			retval = (cosh()).divide(sinh());
			if (isReal()) retval.makeReal();
		} catch (Exception e)
		{
			throw new ArithmeticException("ComplexNumber: coth method numeric exception.");
		}
		
		return retval;
	}
	
/**
 * Returns the hyperbolic secant of the current complex number, 
 * 1 / cosh(z).  
 * If the result is real then the method will force the imaginary part to 0
 * to reduce roundoff error.
 * @return The hyperbolic secant of this.
 * @throws ArithmeticException
 */
	public ComplexNumber sech()   //  sech(this)
	{
		ComplexNumber retval = new ComplexNumber();
		
		try
		{
			retval = (ComplexNumber.makeOne()).divide(cosh());
			if (isReal()) retval.makeReal();
		} catch (Exception e)
		{
			throw new ArithmeticException("ComplexNumber: sech method numeric exception.");
		}
		
		return retval;
	}
	
/**
 * Returns the hyperbolic cosecant of the current complex number, 
 * 1 / sinh(z).  
 * If the result is real then the method will force the imaginary part to 0
 * to reduce roundoff error.
 * @return The hyperbolic cosecant of this.
 * @throws ArithmeticException
 */
	public ComplexNumber csch()   //  csch(this)
	{
		ComplexNumber retval = new ComplexNumber();
		
		try
		{
			retval = (ComplexNumber.makeOne()).divide(sinh());
			if (isReal()) retval.makeReal();
		} catch (Exception e)
		{
			throw new ArithmeticException("ComplexNumber: csch method numeric exception.");
		}
		
		return retval;
	}
	
/**
 * Returns the arcsine of the current complex number, 
 * -i*log(iz+(1-z^2)^(1/2)).  
 * If the result is real then the method will force the imaginary part to 0
 * to reduce roundoff error.
 * @return The arcsine of this.
 * @throws ArithmeticException
 */
	public ComplexNumber asin()   //  asin(this)
	{
		ComplexNumber retval = new ComplexNumber();
		ComplexNumber half = new ComplexNumber(new NumberValue("1", "2"));
		
		try
		{			
			ComplexNumber iz = (ComplexNumber.makeI()).multiply(this);
			ComplexNumber one_z2 = (ComplexNumber.makeOne()).subtract(this.pow(2));
			ComplexNumber sqrt_one_z2 = one_z2.pow(half);
			
			retval = (ComplexNumber.makeMinusI()).multiply((iz.add(sqrt_one_z2)).log());
			if (isReal() && (Math.abs(real.approx()) < 1.0)) retval.makeReal();
		} catch (Exception e)
		{
			throw new ArithmeticException("ComplexNumber: asin method numeric exception.");
		}
		
		return retval;
	}

/**
 * Returns the arccosine of the current complex number, 
 * -i*log(z+i(1-z^2)^(1/2)).  
 * If the result is real then the method will force the imaginary part to 0
 * to reduce roundoff error.
 * @return The arccosine of this.
 * @throws ArithmeticException
 */
	public ComplexNumber acos()   //  acos(this)
	{
		ComplexNumber retval = new ComplexNumber();
		ComplexNumber half = new ComplexNumber(new NumberValue("1", "2"));
		
		try
		{			
			ComplexNumber one_z2 = (ComplexNumber.makeOne()).subtract(this.pow(2));
			ComplexNumber sqrt_one_z2 = one_z2.pow(half);
			ComplexNumber i_sqrt_one_z2 = (ComplexNumber.makeI()).multiply(sqrt_one_z2);
			
			retval = (ComplexNumber.makeMinusI()).multiply((this.add(i_sqrt_one_z2)).log());
			if (isReal() && (Math.abs(real.approx()) < 1.0)) retval.makeReal();
		} catch (Exception e)
		{
			throw new ArithmeticException("ComplexNumber: acos method numeric exception.");
		}
		
		return retval;
	}

/**
 * Returns the arctangent of the current complex number, 
 * 1/(2*I)*log((1+I*z)/(1-I*z)).  
 * If the result is real then the method will force the imaginary part to 0
 * to reduce roundoff error.
 * @return The arctangent of this.
 * @throws ArithmeticException
 */
	public ComplexNumber atan()   //  atan(this)
	{
		ComplexNumber retval = new ComplexNumber();
		ComplexNumber two = new ComplexNumber(new NumberValue("2", "1"));
		
		try
		{			
			//  1/(2*I)*log((1+I*z)/(1-I*z))
			ComplexNumber i_2 = (ComplexNumber.makeOne()).divide((ComplexNumber.makeI()).multiply(two));
			ComplexNumber one_p_iz = (ComplexNumber.makeOne()).add(this.multiply(ComplexNumber.makeI()));
			ComplexNumber one_m_iz = (ComplexNumber.makeOne()).subtract(this.multiply(ComplexNumber.makeI()));
			
			retval = i_2.multiply((one_p_iz.divide(one_m_iz)).log());
			if (isReal()) retval.makeReal();
		} catch (Exception e)
		{
			throw new ArithmeticException("ComplexNumber: atan method numeric exception.");
		}
		
		return retval;
	}

/**
 * Returns the arccotangent of the current complex number, 
 * 1/(2*I)*log((z+i)/(z-i)).  
 * If the result is real then the method will force the imaginary part to 0
 * to reduce roundoff error.
 * @return The arccotangent of this.
 * @throws ArithmeticException
 */
	public ComplexNumber acot()   //  acot(this)
	{
		ComplexNumber retval = new ComplexNumber();
		ComplexNumber two = new ComplexNumber(new NumberValue("2", "1"));
		
		try
		{			
			//  1/(2*I)*log((z+i)/(z-i))
			ComplexNumber i_2 = (ComplexNumber.makeOne()).divide((ComplexNumber.makeI()).multiply(two));
			ComplexNumber z_p_i = this.add(ComplexNumber.makeI());
			ComplexNumber z_m_i = this.subtract(ComplexNumber.makeI());
			
			retval = i_2.multiply((z_p_i.divide(z_m_i)).log());
			if (isReal()) retval.makeReal();
		} catch (Exception e)
		{
			throw new ArithmeticException("ComplexNumber: acot method numeric exception.");
		}
		
		return retval;
	}

/**
 * Returns the arcsecant of the current complex number, 
 * 1/I*log((1-(1-z^2)^(1/2))/z).  
 * If the result is real then the method will force the imaginary part to 0
 * to reduce roundoff error.
 * @return The arcsecant of this.
 * @throws ArithmeticException
 */
	public ComplexNumber asec()   //  asec(this)
	{
		ComplexNumber retval = new ComplexNumber();
		ComplexNumber half = new ComplexNumber(new NumberValue("1", "2"));
		
		try
		{			
			//  1/I*log((1-(1-z^2)^(1/2))/z)
			ComplexNumber inv_i = (ComplexNumber.makeOne()).divide(ComplexNumber.makeI());
			ComplexNumber numz = (ComplexNumber.makeOne()).subtract(((ComplexNumber.makeOne()).subtract(this.pow(2))).pow(half));
			ComplexNumber denz = this;
			
			retval = inv_i.multiply((numz.divide(denz)).log());
			if (isReal() && (Math.abs(real.approx()) > 1.0)) retval.makeReal();
		} catch (Exception e)
		{
			throw new ArithmeticException("ComplexNumber: asec method numeric exception.");
		}
		
		return retval;
	}

/**
 * Returns the arccosecant of the current complex number, 
 * 1/I*log((I+(z^2-1)^(1/2))/z).  
 * If the result is real then the method will force the imaginary part to 0
 * to reduce roundoff error.
 * @return The arccosecant of this.
 * @throws ArithmeticException
 */
	public ComplexNumber acsc()  //  acsc(this)
	{
		ComplexNumber retval = new ComplexNumber();
		ComplexNumber half = new ComplexNumber(new NumberValue("1", "2"));
		
		try
		{			
			//  1/I*log((I+(z^2-1)^(1/2))/z)  
			ComplexNumber inv_i = (ComplexNumber.makeOne()).divide(ComplexNumber.makeI());
			ComplexNumber numz = (ComplexNumber.makeI()).add(((this.pow(2)).subtract(ComplexNumber.makeOne())).pow(half));
			ComplexNumber denz = this;
			
			retval = inv_i.multiply((numz.divide(denz)).log());
			if (isReal() && (Math.abs(real.approx()) > 1.0)) retval.makeReal();
		} catch (Exception e)
		{
			throw new ArithmeticException("ComplexNumber: acsc method numeric exception.");
		}
		
		return retval;
	}

/**
 * Returns the hyperbolic arcsine of the current complex number, 
 * log(z+(z^2+1)^(1/2)).  
 * If the result is real then the method will force the imaginary part to 0
 * to reduce roundoff error.
 * @return The hyperbolic arcsine of this.
 * @throws ArithmeticException
 */
	public ComplexNumber asinh()   //  asinh(this)
	{
		ComplexNumber retval = new ComplexNumber();
		ComplexNumber half = new ComplexNumber(new NumberValue("1", "2"));
		
		try
		{			
			retval = (this.add(((this.pow(2)).add(ComplexNumber.makeOne())).pow(half))).log();
			if (isReal()) retval.makeReal();
		} catch (Exception e)
		{
			throw new ArithmeticException("ComplexNumber: asinh method numeric exception.");
		}
		
		return retval;
	}

/**
 * Returns the hyperbolic arccosine of the current complex number, 
 * log(z+(z^2-1)^(1/2)).  
 * If the result is real then the method will force the imaginary part to 0
 * to reduce roundoff error.
 * @return The hyperbolic arccosine of this.
 * @throws ArithmeticException
 */
	public ComplexNumber acosh()   //  acosh(this)
	{
		ComplexNumber retval = new ComplexNumber();
		ComplexNumber half = new ComplexNumber(new NumberValue("1", "2"));
		
		try
		{			
			retval = (this.add(((this.pow(2)).subtract(ComplexNumber.makeOne())).pow(half))).log();
			if (isReal() && (Math.abs(real.approx()) > 1.0)) retval.makeReal();
		} catch (Exception e)
		{
			throw new ArithmeticException("ComplexNumber: acosh method numeric exception.");
		}
		
		return retval;
	}

/**
 * Returns the hyperbolic arctangent of the current complex number, 
 * 1/2*log((1+z)/(1-z)).  
 * If the result is real then the method will force the imaginary part to 0
 * to reduce roundoff error.
 * @return The hyperbolic arctangent of this.
 * @throws ArithmeticException
 */
	public ComplexNumber atanh()   //  atanh(this)
	{
		ComplexNumber retval = new ComplexNumber();
		ComplexNumber half = new ComplexNumber(new NumberValue("1", "2"));
		
		try
		{			
			retval = half.multiply((((ComplexNumber.makeOne()).add(this)).divide((ComplexNumber.makeOne()).subtract(this))).log());
			if (isReal() && (Math.abs(real.approx()) < 1.0)) retval.makeReal();
		} catch (Exception e)
		{
			throw new ArithmeticException("ComplexNumber: atanh method numeric exception.");
		}
		
		return retval;
	}

/**
 * Returns the hyperbolic arccotangent of the current complex number, 
 * 1/2*log((1+z)/(z-1)).  
 * If the result is real then the method will force the imaginary part to 0
 * to reduce roundoff error.
 * @return The hyperbolic arccotangent of this.
 * @throws ArithmeticException
 */
	public ComplexNumber acoth()   //  acoth(this)
	{
		ComplexNumber retval = new ComplexNumber();
		ComplexNumber half = new ComplexNumber(new NumberValue("1", "2"));
		
		try
		{			
			retval = half.multiply((((ComplexNumber.makeOne()).add(this)).divide(this.subtract(ComplexNumber.makeOne()))).log());
			if (isReal() && (Math.abs(real.approx()) > 1.0)) retval.makeReal();
		} catch (Exception e)
		{
			throw new ArithmeticException("ComplexNumber: acoth method numeric exception.");
		}
		
		return retval;
	}

/**
 * Returns the hyperbolic arcsecant of the current complex number, 
 * log((1+(1-z^2)^(1/2))/z).  
 * If the result is real then the method will force the imaginary part to 0
 * to reduce roundoff error.
 * @return The hyperbolic arcsecant of this.
 * @throws ArithmeticException
 */
	public ComplexNumber asech()   //  asech(this)
	{
		ComplexNumber retval = new ComplexNumber();
		ComplexNumber half = new ComplexNumber(new NumberValue("1", "2"));
		
		try
		{			
			retval = (((ComplexNumber.makeOne()).add(((ComplexNumber.makeOne()).subtract(this.pow(2))).pow(half))).divide(this)).log();
			if (isReal() && (Math.abs(real.approx()) < 1.0) && (Math.abs(real.approx()) > 0.0)) retval.makeReal();
		} catch (Exception e)
		{
			throw new ArithmeticException("ComplexNumber: asech method numeric exception.");
		}
		
		return retval;
	}

/**
 * Returns the hyperbolic arccosecant of the current complex number, 
 * log((1+(1+z^2)^(1/2))/z).  
 * If the result is real then the method will force the imaginary part to 0
 * to reduce roundoff error.
 * @return The hyperbolic arccosecant of this.
 * @throws ArithmeticException
 */
	public ComplexNumber acsch()   //  acsch(this)
	{
		ComplexNumber retval = new ComplexNumber();
		ComplexNumber half = new ComplexNumber(new NumberValue("1", "2"));
		
		try
		{			
			retval = (((ComplexNumber.makeOne()).add(((ComplexNumber.makeOne()).add(this.pow(2))).pow(half))).divide(this)).log();
			if (isReal() && (!real.isZero())) retval.makeReal();
		} catch (Exception e)
		{
			throw new ArithmeticException("ComplexNumber: acsch method numeric exception.");
		}
		
		return retval;
	}

/**
 * Returns the sign of the real part of the current complex number.  If r > 0
 * the method returns 1, if r < 0 the method returns -1 and if r = 0 the method
 * returns 0.
 * @return The sign of the real part of this.
 * @throws ArithmeticException
 */
	public ComplexNumber sign()   //  sign(this)  For real values.
	{
		ComplexNumber retval = new ComplexNumber();

		try
		{	
			if (real.isZero())		
				retval = ComplexNumber.makeZero();
			else if (real.approx() > 0)
				retval = ComplexNumber.makeOne();
			else
				retval = ComplexNumber.makeMinusOne();			
		} catch (Exception e)
		{
			throw new ArithmeticException("ComplexNumber: sign method numeric exception.");
		}
		
		return retval;
	}

/**
 * If the current complex number is not zero this method returns 0 and if the current
 * complex number is zero this method throws an ArithmeticException.
 * @return 0 if the current complex number is not zero and throws an 
 * ArithmeticException if the current complex number is zero.
 * @throws ArithmeticException
 */
	public ComplexNumber zeron()   //  zeron(this)
	{
		ComplexNumber retval = ComplexNumber.makeZero();
		
		if (isZero())
			throw new ArithmeticException("ComplexNumber: zeron method numeric exception.");
		
		return retval;
	}
	
/**
 * Clones the current complex number, note that the return type is ComplexNumber 
 * and not Object. 	
 */
	public synchronized ComplexNumber clone()
	{
		return new ComplexNumber(real.clone(), imag.clone());
	}	

/**
 * Returns a string representation of the current complex number.
 * @return A string representation of the current complex number.
 */
	public String toString()
	{
		String retstr = "";

		if (!real.isZero())
			retstr += real.toString();
			
		if (!imag.isZero())
			if (!real.isZero())
			{
				if (imag.approx() < 0)
				{
					if (imag.negate().isOne())
						retstr += " - i";
					else
						retstr += " - " + imag.negate().toString() + "i";
				}
				else
				{
					if (imag.isOne())
						retstr += " + i";
					else
						retstr += " + " + imag.toString() + "i";
				}
			}
			else
			{
				if (imag.isOne())
					retstr += "i";
				else if (imag.negate().isOne())
					retstr += "-i";
				else
					retstr += imag.toString() + "i";
			}

		if (real.isZero() && imag.isZero())
			retstr = "0";

		return retstr;
	}

/**
 * Returns a LaTeX string representation of the current complex number.
 * @return A LaTeX string representation of the current complex number.
 */
	public String toLaTeXString()
	{
		String retstr = "";
		
		if (!real.isZero())
			retstr += real.toLaTeXString();
			
		if (!imag.isZero())
			if (!real.isZero())
			{
				if (imag.approx() < 0)
				{
					if (imag.negate().isOne())
						retstr += " - i";
					else
						retstr += " - " + imag.negate().toLaTeXString() + "i";
				}
				else
				{
					if (imag.isOne())
						retstr += " + i";
					else
						retstr += " + " + imag.toLaTeXString() + "i";
				}
			}
			else
			{
				if (imag.isOne())
					retstr += "i";
				else if (imag.negate().isOne())
					retstr += "-i";
				else
					retstr += imag.toLaTeXString() + "i";
			}

		if (real.isZero() && imag.isZero())
			retstr = "0";

		return retstr;
	}

/**
 * Returns a code string representation of the current complex number. The difference
 * between this and the {@link #toString() toString()} method is that the result can be read into the
 * parsers of the {@link ExpFormula ExpFormula} class. 
 * @return A code string representation of the current complex number.
 */
	public String toCodeString()
	{
		String retstr = "";
		
		if (!real.isZero())
			retstr += real.toString();
			
		if (!imag.isZero())
			if (!real.isZero())
			{
				if (imag.approx() < 0)
				{
					if (imag.negate().isOne())
						retstr += " - i";
					else
						retstr += " - " + imag.negate().toString() + "*i";
				}
				else
				{
					if (imag.isOne())
						retstr += " + i";
					else
						retstr += " + " + imag.toString() + "*i";
				}
			}
			else
			{
				if (imag.isOne())
					retstr += "i";
				else if (imag.negate().isOne())
					retstr += "-i";
				else
					retstr += imag.toString() + "*i";
			}

		if (real.isZero() && imag.isZero())
			retstr = "0";

		return retstr;
	}

/**
 * Returns a Maple code string representation of the current complex number.
 * @return A Maple code string representation of the current complex number.
 */
	public String toMapleCodeString()
	{
		String retstr = "";
		
		if (!real.isZero())
			retstr += real.toString();
			
		if (!imag.isZero())
			if (!real.isZero())
			{
				if (imag.approx() < 0)
				{
					if (imag.negate().isOne())
						retstr += " - I";
					else
						retstr += " - " + imag.negate().toString() + "*I";
				}
				else
				{
					if (imag.isOne())
						retstr += " + I";
					else
						retstr += " + " + imag.toString() + "*I";
				}
			}
			else
			{
				if (imag.isOne())
					retstr += "I";
				else if (imag.negate().isOne())
					retstr += "- I";
				else
					retstr += imag.toString() + "*I";
			}

		if (real.isZero() && imag.isZero())
			retstr = "0";

		return retstr;
	}

	/**
	 * Returns a Maxima code string representation of the current complex number.
	 * @return A Maxima code string representation of the current complex number.
	 */
		public String toMaximaCodeString()
		{
			String retstr = "";
			
			if (!real.isZero())
				retstr += real.toString();
				
			if (!imag.isZero())
				if (!real.isZero())
				{
					if (imag.approx() < 0)
					{
						if (imag.negate().isOne())
							retstr += " - %i";
						else
							retstr += " - " + imag.negate().toString() + "*%i";
					}
					else
					{
						if (imag.isOne())
							retstr += " + %i";
						else
							retstr += " + " + imag.toString() + "*%i";
					}
				}
				else
				{
					if (imag.isOne())
						retstr += "%i";
					else if (imag.negate().isOne())
						retstr += "- %i";
					else
						retstr += imag.toString() + "*%i";
				}

			if (real.isZero() && imag.isZero())
				retstr = "0";

			return retstr;
		}

/**
 * Returns a Mathematica code string representation of the current complex number.
 * @return A Mathematica code string representation of the current complex number.
 */
	public String toMathematicaCodeString()
	{
		String retstr = "";
		
		if (!real.isZero())
			retstr += real.toString();
			
		if (!imag.isZero())
			if (!real.isZero())
			{
				if (imag.approx() < 0)
				{
					if (imag.negate().isOne())
						retstr += " - I";
					else
						retstr += " - " + imag.negate().toString() + " I";
				}
				else
				{
					if (imag.isOne())
						retstr += " + I";
					else
						retstr += " + " + imag.toString() + " I";
				}
			}
			else
			{
				if (imag.isOne())
					retstr += "I";
				else if (imag.negate().isOne())
					retstr += "- I";
				else
					retstr += imag.toString() + " I";
			}

		if (real.isZero() && imag.isZero())
			retstr = "0";

		return retstr;
	}

/**
 * Returns the modulus of the current complex number as a double.
 * @return The modulus of the current complex number as a double.
 */
	public double mod()
	{
		return Math.sqrt(real.approx()*real.approx()+imag.approx()*imag.approx());
	}

/**
 * Returns the argument of the current complex number as a double.
 * @return The argument of the current complex number as a double.
 */
	public double arg()
	{
		return Math.atan2(imag.approx(), real.approx());
	}
	
/**
 * Determines if the two complex numbers are equal.
 * @return true if the two complex numbers are equal and false otherwise.
 */
	public boolean equals(ComplexNumber c)
	{
		return (real.equals(c.getReal()) && imag.equals(c.getImag()));
	}

/**
 * If the complex number is close to 0 as defined by the {@link NumericTolerances
 * NumericTolerances} parameter tol the return value is the complex number 0.  If 
 * the current complex number is not close to 0 as defined by tol the return value
 * is the value of the current complex number. 
 * @param tol {@link NumericTolerances NumericTolerances} type.
 * @return 0 if the current complex number is close to 0 and the current 
 * complex number otherwise.
 */
	public ComplexNumber adjustToZero(NumericTolerances tol)
	{
		if (isZero(tol))
			return ComplexNumber.makeZero();
		else
			return this.clone();
	}

/**
 * This method will see if either the real or imaginary parts of the complex number
 * are close to 0 as defined by the {@link NumericTolerances
 * NumericTolerances} parameter tol.  If either are then those close to 0 are set 
 * to 0, otherwise the value is unaltered.  
 * @param tol {@link NumericTolerances NumericTolerances} type.
 * @return The complex number with components that are adjusted to 0 if the component 
 * is close to 0.
 */
	public ComplexNumber adjustComponentsToZero(NumericTolerances tol)
	{
		NumberValue r = getReal();
		NumberValue i = getImag();
		
		if (r.isZero(tol))
			r = NumberValue.makeZero();
			
		if (i.isZero(tol))
			i = NumberValue.makeZero();

		return new ComplexNumber(r, i);	
	}

/**
 * This method will return 0 if the current complex number is close to 0, 1 if the
 * current complex number is close to 1, and -1 if the current complex number is 
 * close to -1, as defined by the {@link NumericTolerances
 * NumericTolerances} parameter tol.  Otherwise the return valu is the current 
 * complex number.  
 * @param tol {@link NumericTolerances NumericTolerances} type.
 * @return 0, 1, or -1 if the current complex number is close to these and the 
 * complex number otherwise.
 */
	public ComplexNumber adjust(NumericTolerances tol)
	{
		if (isZero(tol))
			return ComplexNumber.makeZero();
		if (isOne(tol))
			return ComplexNumber.makeOne();
		if (isMinusOne(tol))
			return ComplexNumber.makeMinusOne();
		else
			return this.clone();
	}

/**
 * This method returns the factorial of the long integer n as an exact 
 * complex number. 
 * @param n long integer.
 * @return n!
 */
	public static ComplexNumber factorial(long n)
	{
		return new ComplexNumber(NumberValue.factorial(n), NumberValue.makeZero());
	}

}