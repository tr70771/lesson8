package expFormula;

/**
 * The ExpTreenode class is the node structure for the binary expression trees
 * used in the {@link ExpFormula ExpFormula} class.  If you are using the ExpFormula
 * class in your application to give it function evaluation and differentiation 
 * capabilities you should never need to use this class.  On the other hand, if you 
 * are extending the  ExpFormula class to give it more functionality you may need to 
 * access the tree and nodes directly. 
 * @author Don Spickler
 * @version 1.2
 */
public class ExpTreenode implements Cloneable 
{
	/**
	 * This is an operation string.  Its values are either a binary (+, -, *, /, ^)
	 * operation, a unary (-) operation, a function (sin, cos, ...), the # symbol, or
	 * a variable.  If this
	 * string is a binary operation then both left and right children will not be null
	 * but if the operation is unary or a function the left child will be null.
	 * If the operation is the # then the children will both be null and the num field 
	 * will have a ComplexNumber in it.  If the operation is a variable then both 
	 * children and num will be null. 
	 */
	public String op_str; 
	
	/**
	 * This holds a complex number if the op_str field contains a #.
	 */
	public ComplexNumber num;
	
	/**
	 * The left child of the tree node, that is the left operand.
	 */
	public ExpTreenode left;

	/**
	 * The right child of the tree node, that is the right operand or the unary operand.
	 */
	public ExpTreenode right;

	/**
	 * This is a boolean to enable the ExpFormula parser to reverse the order of exponentiation.
	 * That is, the input of x^x^x will parse into x^(x^x) instead of (x^x)^x.
	 */
	public boolean expon = false;

	/**
	 * Constructor that places s into the op_str field and null for num and the children.
	 * @param s the string for op_str.
	 */
	public ExpTreenode(final String s)
	{
		this(s, null, null, null);
	}

	/**
	 * This constructor places n into num, # into the op_str and null for both children. 
	 * @param n the ComplexNumber to be stored.
	 */
	public ExpTreenode(final ComplexNumber n)
	{
		this("#", n, null, null);
	}

	/**
	 * This constructor fills the node with all parameters.  Note that neither the 
	 * children nor num are cloned.   Also, expon is set to the default of false.
	 * @param s the op_str.
	 * @param n the complex number.
	 * @param l the left child.
	 * @param r the right child.
	 */
	public ExpTreenode(final String s, final ComplexNumber n, final ExpTreenode l, final ExpTreenode r)
	{
		op_str = s;
		num = n;
		left = l;
		right = r;
		expon = false;
	}

	/**
	 * This constructor fills the node with all parameters.  Note that neither the 
	 * children nor num are cloned.  
	 * @param s the op_str.
	 * @param e the expon.
	 * @param n the complex number.
	 * @param l the left child.
	 * @param r the right child.
	 */
	public ExpTreenode(final String s, final boolean e, final ComplexNumber n, final ExpTreenode l, final ExpTreenode r)
	{
		op_str = s;
		num = n;
		left = l;
		right = r;
		expon = e;
	}

	/**
	 * This method clones the entire expression tree.
	 */
	public synchronized ExpTreenode clone()
	{
		if (num != null)
			return new ExpTreenode(op_str, expon, num.clone(), (left != null ? (ExpTreenode)left.clone() : null), (right != null ? (ExpTreenode)right.clone() : null));
		else
			return new ExpTreenode(op_str, expon, null, (left != null ? (ExpTreenode)left.clone() : null), (right != null ? (ExpTreenode)right.clone() : null));
	}	
	
	/**
	 * Determines if the current expression tree equals the input tree t.
	 * @param t the input expression tree to be checked with the current tree.
	 * @return true if the trees are identical and false otherwise. 
	 */
	public boolean equals(ExpTreenode t)
	{
		if (t == null) return false;
		
		boolean leftEqual = false;
		boolean rightEqual = false;
		
		if ((left != null) && (t.left != null))
			leftEqual = left.equals(t.left);
		else if ((left == null) && (t.left == null))
			leftEqual = true;

		if ((right != null) && (t.right != null))
			rightEqual = right.equals(t.right);
		else if ((right == null) && (t.right == null))
			rightEqual = true;
		
		return leftEqual && rightEqual && op_str.equalsIgnoreCase(t.op_str) && num.equals(t.num) && (expon == t.expon);
	}
}