package com.learnopengles.android.lesson8;

import android.content.Context;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;
import android.util.FloatMath;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.Toast;

//import com.example.dmath.Constant;
//import com.example.dmath.LessonOneRenderer;
import com.learnopengles.android.R;

public class LessonEightGLSurfaceView extends GLSurfaceView implements ErrorHandler
{	
	private LessonEightRenderer renderer;
	
	/**/
	// These matrices will be used to move and zoom image
	   Matrix matrix = new Matrix();
	   Matrix savedMatrix = new Matrix();
	
	   // We can be in one of these 3 states
	   static final int NONE = 0;
	   static final int DRAG = 1;
	   static final int ZOOM = 2;
	   int mode = NONE;
	
	   // Remember some things for zooming
	   PointF start = new PointF();
	   PointF mid = new PointF();
	   float oldDist = 1f;
	   
	   private static final String TAG = "Touch";
	/**/
	
	// Offsets for touch events	 
    private float previousX;
    private float previousY;
    
    private float density;
        	
	public LessonEightGLSurfaceView(Context context) 
	{
		super(context);		
	}
	
	public LessonEightGLSurfaceView(Context context, AttributeSet attrs) 
	{
		super(context, attrs);		
	}
	
	@Override
	public void handleError(final ErrorType errorType, final String cause) {
		// Queue on UI thread.
		post(new Runnable() {
			@Override
			public void run() {
				final String text;

				switch (errorType) {
				case BUFFER_CREATION_ERROR:
					text = String
							.format(getContext().getResources().getString(
									R.string.lesson_eight_error_could_not_create_vbo), cause);
					break;
				default:
					text = String.format(
							getContext().getResources().getString(
									R.string.lesson_eight_error_unknown), cause);
				}

				Toast.makeText(getContext(), text, Toast.LENGTH_LONG).show();

			}
		});
	}
/*
	@Override
	public boolean onTouchEvent(MotionEvent event) 
	{
		if (event != null)
		{			
			float x = event.getX();
			float y = event.getY();
			
			if (event.getAction() == MotionEvent.ACTION_MOVE)
			{
				if (renderer != null)
				{
					float deltaX = (x - previousX) / density / 2f;
					float deltaY = (y - previousY) / density / 2f;
					
					renderer.deltaX += deltaX;
					renderer.deltaY += deltaY;												
				}
			}	
			
			previousX = x;
			previousY = y;
			
			return true;
		}
		else
		{
			return super.onTouchEvent(event);
		}		
	}
*/
	
	
	@Override
	public boolean onTouchEvent(MotionEvent event) 
	{
		if (event != null)
		{
			float x = event.getX();
			float y = event.getY();
			
			switch (event.getAction() & MotionEvent.ACTION_MASK)
			{
				case MotionEvent.ACTION_POINTER_DOWN:
					oldDist = spacing(event);
					Log.d(TAG, "oldDist=" + oldDist);
					if (oldDist > 10f)
					{
						////savedMatrix.set(matrix);
						midPoint(mid, event);
						mode = ZOOM;
						Log.d(TAG, "mode=ACTION_POINTER_DOWN");
					}
					switch (event.getAction() & MotionEvent.ACTION_MASK)
					{
						case MotionEvent.ACTION_POINTER_DOWN:
								Log.d(TAG, "mode=ACTION_POINTER_DOWN 2");
						break;
					}
				break;
				case MotionEvent.ACTION_DOWN:
					mode = DRAG;
					Log.d(TAG, "mode=ACTION_DOWN");
				break;
				case MotionEvent.ACTION_UP:
					Log.d(TAG, "mode=ACTION_UP");
				break;
				case MotionEvent.ACTION_POINTER_UP:
					mode = NONE;
					// Log.d(TAG, "mode=NONE");
					Log.d(TAG, "mode=ACTION_POINTER_UP");
				break;
				case MotionEvent.ACTION_MOVE:
					if (mode == DRAG)
			        {
						if (renderer != null)
						{
							float deltaX = (x - previousX)/ density / 2f;
							float deltaY = (y - previousY)/ density / 2f;
							
							renderer.deltaX += deltaX;
							renderer.deltaY += deltaY;												
						}
			        }
			        if (mode == ZOOM)
			        {
			        	if (renderer != null)
						{
			        		float newDist = spacing(event);
			        		Log.d(TAG, "mode=ZOOM");
				           
			        		// Limiting zoom range; find an alternate way
			        		if (newDist > 10f && renderer.deltaZ >= 0.1f && renderer.deltaZ <= 100.0f)
			        		{
			        			//Log.d(TAG, "deltaZ=" + mRenderer.deltaZ);
			        			//matrix.set(savedMatrix);
			        			float scale = newDist / oldDist;
			        			float scale2;
			        			if(scale >= 1.0f) // Zoom out needs to be negative
			        			{
			        				scale2 = -1.0f * (oldDist/newDist);
			        				renderer.deltaZ += (scale2 / 10);
			        			}
			        			else 
			        			{
			        				//Log.d(TAG, "change=" + (scale / 10f));
			        				renderer.deltaZ += (scale / 10);
			        			}
			        		}
			        		//Log.d(TAG, "distance=" + renderer.deltaZ);
			        		else
/**/		        		{
			        			if (renderer.deltaZ < 0.1f)
			        				renderer.deltaZ = 0.1f; // NEAR
			        			else if (renderer.deltaZ > 100.0f)
			        				renderer.deltaZ = 100.0f; //FAR
			        		}
/**/					}
			        }
			    break;
			}
			
			previousX = x;
			previousY = y;
			
			return true;
		}
		else
		{
			return super.onTouchEvent(event);
		}
	}
	
	// Hides superclass method.
	public void setRenderer(LessonEightRenderer renderer, float density) 
	{
		this.renderer = renderer;
		this.density = density;
		super.setRenderer(renderer);
	}
	/* Determine the space between the first two fingers */
	private float spacing(MotionEvent event)
	{
		// added if statement to prevent error:
		// 	"pointerIndex out of range"
		if(event.getPointerCount() == 2)
		{
			float x = event.getX(0) - event.getX(1);
			float y = event.getY(0) - event.getY(1);
			return FloatMath.sqrt(x * x + y * y);
		}
		return 0;
	}
	
	/* Calculate the mid point of the first two fingers */
	private void midPoint(PointF point, MotionEvent event)
	{
		float x = event.getX(0) + event.getX(1);
		float y = event.getY(0) + event.getY(1);
		point.set(x / 2, y / 2);
	}
}
	
	
/*	
	// Hides superclass method.
	public void setRenderer(LessonEightRenderer renderer, float density) 
	{
		this.renderer = renderer;
		this.density = density;
		super.setRenderer(renderer);
	}
}
*/
